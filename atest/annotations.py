from types import FunctionType
from .runner import _MAIN, _DICT


def test(func: FunctionType):
    if str(type(func)) !="<class 'function'>":
        raise ValueError("Annotation test must be used only with functions! For class methods use annotation test_class")
    _DICT[func.__module__].append(func)
    _MAIN.append(func)